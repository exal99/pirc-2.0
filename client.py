import pirc
import interface
import curses
import liers_dice
import random
import time

# Host and port
HOST = "irc.patwic.com"
PORT = 6667

# IRC server password
PASSWORD = "foobar"

# Create IRC server connection object
irc = pirc.IRC(HOST, PORT, PASSWORD)

# User data
NICK = "Liers"
REALNAME = "LieOrDie"

# Channel
CHAN = "#patwic"

def send_msg(msg):
    if msg.startswith('/'):
        return msg[1:]
    elif msg.startswith('@'):
        reciver = msg[1:msg.index(' ')]
        real_msg = msg[msg.index(' '):]
        return 'PRIVMSG ' + reciver + ':' + real_msg
    else:
        return 'PRIVMSG #patwic :' + msg

def get_from(msg):
    return msg[1:msg.index(']')]

def get_msg(msg):
    return format_msg(msg)[format_msg(msg).index(':') + 1:]
    
def format_msg(msg):
    try:
        usr = msg[1:msg.index('!')]
        #usr = get_from(msg)
        to = msg[msg.index('PRIVMSG ') + 8:]
        return '[' + usr + '] ' + to 
    except ValueError:
        return msg

def format_usr_msg(msg):
    if msg.startswith('PRIVMSG '):
        to = msg[8:msg[9:].index(' ') + 9]
        real_msg = msg[msg.index(to) + len(to):]
        return real_msg[2:] + ' --> ' + to

def start_new_game(msg):
    return get_msg(msg).lower().startswith('new game')

def join_game(msg, players):
    return get_msg(msg).lower().startswith('join game') and \
    not get_form(msg) in [player.name for player in players]

def send_join_msg(name, party_leader = False):
    if party_leader:
        return 'PRIVMSG #patwic :' + name + ' has started a new game and is now the party leader!'
    else:
        return 'PRIVMSG #patwic :' + name + ' has joined the game'

def start_game(msg, party_leader):
    return get_from(msg) == party_leader.name and get_msg(msg).lower().startswith('start game')

def send_order(players):
    str_list = [str(player) for player in players]
    return 'PRIVMSG #patwic :The order are as folows: ' + ', '.join(str_list)
    
def send_dice_roll(players, ui):
    for player in players:
        msg = player.send_msg()
        irc.send(msg)
        ui.output(format_msg(msg))
        
def joined_players(msg):
    return get_msg(msg).lower().startswith('players')

def send_players(players):
    str_list = [str(player) for player in players]
    return 'PRIVMSG #patwic :Players joined so far: ' + ', '.join(str_list)

def isbet(msg):
    return get_msg(msg).lower().startswith('bet')

def valid_bet(bet, best_bet):
    return bet > best_bet
    
def send_bet(bet):
    return 'PRIVMSG #patwic :The current bet is: ' + bet + '. Truth or lie?'

def get_bet(msg):
    msg = msg.lower()
    return msg[msg.index('bet ') + 4:]

def add_dice_to_list(current_rolls, roll):
    in_list = False
    for index in range(len(current_rolls)):
        if current_rolls[index][-1] == roll[-1]:
            new_num_of = int(current_rolls[index][:-2]) + int(roll[:-2])
            current_rolls[index] = str(new_num_of) + current_rolls[index][-2:]
            in_list = True
            break
    if not in_list:
        current_rolls.append(roll)
    return current_rolls
    
def update_roll_list(current_list, list_to_add):
    for roll in list_to_add:
        current_list = add_dice_to_list(current_list, roll)
    return current_list

def new_current_rolls(players):
    return_list = []
    for player in players:
        return_list = update_roll_list(return_list, player.dice_rolls)
    return return_list

def correct_bet(current_rolls, bet):
    if bet in current_rolls:
        return True
    else:
        for roll in current_rolls:
            if roll[-1] == bet[-1]:
                if bet <= roll:
                    return True
                else:
                    return False
        return False

def lier_call(msg):
    return get_msg(msg).lower().startswith('lier')

def remove_dice_from_player(players, player_name):
    for player in players:
        if player.name == player_name:
            player.remove_dice()
            break

def send_all_dices(rolls):
    rolls.sort()
    return 'PRIVMSG #patwic :Rolled dices: ' + ', '.join(rolls)

def update_players(players):
    for player in players:
        if player.num_dice == 0:
            players.remove(player)
            break
    return players
    
def get_help(msg):
    return get_msg(msg).lower().startswith('help')

def get_help_new_game():
    return "PRIVMSG #patwic :If you would like to start a new game type: 'new game'. If you \
want to know about liers dice pleace vist, http://www.wikihow.com/Play-Liar%27s-Dice, for more information.\
 More information will be avalible once you start the game."

def get_help_joining():
    return "PRIVMSG #patwic :The game has started and to join simply type: 'join game'. Make sure\
 you understand the rules for liers dice. They can be found at, http://www.wikihow.com/Play-Liar%27s-Dice."
 
def get_help_bet():
    return "PRIVMSG #patwic :It's now time to place bets. To place a bet type: 'bet <your bet>'.\
 An example of a bet is 5-3 meaning that you bet that there is 5 3's on the table. If you don't\
 want to bet you could call lier by simply typing 'lier'. All bets need to be higer than previus ones."

def kick(msg, party_leader):
    return party_leader.name == get_from(msg) and get_msg(msg).lower().startswith('kick')
    
def kick_player(players, player_to_kick):
    #player_to_kick = msg[msg.index('kick ') + 5:]
    for player in players:
        if player.name == player_to_kick:
            players.remove(player)
            break

def kick_msg(player):
    return 'PRIVMSG #patwic :%s has been kicked by admin/party leader' %(player)

def main(stdscreen):

    ui = interface.TextUI(stdscreen)
    
    can_join = False
    started = False
    players = []
    party_leader = None
    turn = 0
    greatest_bet = ('0-0', None)
    dice_rolls = []

    while True:
        try:
            msg = irc.read()
            if msg:
                msg = format_msg(msg)
                ui.set_text_color(interface.YELLOW)
                ui.output(msg)
                #ui.output(get_msg(msg))
                ui.set_text_color(interface.WHITE)
                
                if not started and not can_join: #new game
                    #game_started = start_game(msg)
                    can_join = start_new_game(msg)
                    #if game_started:
                    if can_join: #if game has been created
                        starter = get_from(msg) #who stated the game
                        player = liers_dice.Player(starter, party_leader=True) #creates a new player
                        players.append(player)
                        party_leader = player
                        irc.send(send_join_msg(starter, party_leader=True)) #sends the msg about new game
                        ui.output(format_usr_msg(send_join_msg(starter, party_leader=True))) #prints to bot screen as well
                    if get_help(msg):
                        irc.send(get_help_new_game())
                        ui.output(format_usr_msg(get_help_new_game()))
                        
                elif can_join: #joining face
                    if join_game(msg, players): #if someone says 'join game'
                        joiner = get_from(msg) #the one who said it
                        players.append(liers_dice.Player(joiner))
                        irc.send(send_join_msg(joiner)) #sends msg about someone joining
                        ui.output(format_usr_msg(send_join_msg(joiner))) #prints for bot
                    elif start_game(msg, party_leader): #if the party leader sais 'start game'
                        can_join = False #none else can join
                        started = True #the game has started
                        random.shuffle(players) #shuffles the order
                        irc.send(send_order(players)) #sends the order
                        ui.output(format_usr_msg(send_order(players)))
                        time.sleep(2) 
                        send_dice_roll(players, ui) #sends the dice roll to all players
                        dice_rolls = new_current_rolls(players)
                    elif kick(msg, party_leader):
                        player_to_kick = msg[msg.index('kick ') + 5:]
                        kick_player(players, player_to_kick)
                        irc.send(kick_msg(player_to_kick))
                        ui.output(format_usr_msg(kick_msg(player_to_kick)))
                        
                    elif joined_players(msg): #if someone says 'players' (shows all joined players)
                        irc.send(send_players(players))
                        ui.output(format_usr_msg(send_players(players)))
                    elif get_help(msg): #someone whrites 'help'
                        irc.send(get_help_joining())
                        ui.output(format_usr_msg(get_help_joining()))
                elif started:
                    if isbet(msg) and get_from(msg) == players[turn % len(players)].name:
                        bet = get_bet(msg)
                        if valid_bet(bet, greatest_bet[0]):
                            greatest_bet = (bet, get_from(msg))
                            turn += 1
                            irc.send(send_bet(bet))
                            ui.output(format_usr_msg(send_bet(bet)))
                    elif lier_call(msg) and get_from(msg) == players[turn % len(players)].name:
                        if correct_bet(dice_rolls, greatest_bet[0]):
                            irc.send(send_all_dices(dice_rolls))
                            ui.output(format_usr_msg(send_all_dices(dice_rolls)))
                            remove_dice_from_player(players, get_from(msg))
                            to_send = 'PRIVMSG #patwic :%s called %s a lier. %s loses a dice.' \
                                            %(get_from(msg), greatest_bet[1], get_from(msg))
                            irc.send(to_send)
                            ui.output(format_usr_msg(to_send))
                            
                            players = update_players(players)
                            turn = 0
                            greatest_bet = ('0-0', None)
                            dice_rolls = []
                            send_dice_roll(players, ui) #sends the dice roll to all players
                            dice_rolls = new_current_rolls(players)
                            if len(players) == 0:
                                to_send = 'PRIVMSG #patwic :And the winner is: %s! Congratulation' %s(players[0].name)
                                irc.send(to_send)
                                ui.output(format_usr_msg(to_send))
                                can_join = False
                                started = False
                                players = []
                                dice_rolls = []
                                
                        else:
                            remove_dice_from_player(players, greatest_bet[1])
                            irc.send(send_all_dices(dice_rolls))
                            ui.output(format_usr_msg(send_all_dices(dice_rolls)))
                            to_send = 'PRIVMSG #patwic :%s called %s a lier. %s loses a dice.' \
                                        %(get_from(msg), greatest_bet[1], greatest_bet[1])
                            irc.send(to_send)
                            ui.output(format_usr_msg(to_send))
                            
                            players = update_players(players)
                            turn = 0
                            greatest_bet = ('0-0', None)
                            dice_rolls = []
                            send_dice_roll(players, ui) #sends the dice roll to all players
                            dice_rolls = new_current_rolls(players)
                            if len(players) == 1:
                                to_send = 'PRIVMSG #patwic :And the winner is: %s! Congratulation' %(players[0].name)
                                irc.send(to_send)
                                ui.output(format_usr_msg(to_send))
                                can_join = False
                                started = False
                                players = []
                                dice_rolls = []
                    elif get_help(msg):
                        irc.send(get_help_bet())
                        ui.output(format_usr_msg(get_help_bet()))
                    
            usr_msg = ui.input()
            if usr_msg:
                ui.set_text_color(interface.CYAN)
                usr_msg = send_msg(usr_msg)
                irc.send(usr_msg)
                ui.output(format_usr_msg(usr_msg))
                ui.set_text_color(interface.WHITE)
        
        except EOFError:
            return
        except KeyboardInterrupt:
            return
if __name__ == '__main__':
	# Connect to server and register user
	irc.connect(NICK, REALNAME)
	
	# Join channel
	irc.send("JOIN " + CHAN)
	
	curses.wrapper(main)
