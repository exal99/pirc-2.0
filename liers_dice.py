import random

class Player(object):
    
    def __init__(self, name, party_leader = False):
        self.num_dice = 5
        self.name = name
        self.party_leader = party_leader
        self.dice_rolls = []
    
    def remove_dice(self):
        self.num_dice -= 1
    
    def throw_dice(self):
        result = {}
        for throw in range(self.num_dice):
            dice_result = random.randint(1, 6)
            if dice_result in result:
                result[dice_result] += 1
            else:
                result[dice_result] = 1
        return result
    
    def send_msg(self):
        return_msg = 'PRIVMSG ' + self.name + ' :You got: '
        dice_roll = self.throw_dice()
        result = [str(dice_roll[dice_num]) + '-' + str(dice_num) for dice_num in dice_roll]
        result.sort()
        self.dice_rolls = result
        return_msg += ', '.join(result)
        return return_msg
        
    
    def __str__(self):
        return self.name
        
